/*
*Assignment Data.0
*/

/*Write an SQL statement that reads all columns from 'AFG', Afghanistan.*/

select *
from countrylanguage
where countrycode = 'AFG';

/*Write an SQL statement that reads all columns from 'TJK', Tajikistan*/

select *
from countrylanguage
where countrycode = 'TJK';

/*Write an SQL statement that reads all columns from 'UZB', Uzbekistan*/

select *
from countrylanguage
where countrycode = 'UZB';

/*Write an SQL statement that reads all columns from 'UZB', 'TJK', and 'AFG'*/

select *
from countrylanguage
where countrycode = 'UZB', 'TJK', 'AFG';

/*Now, verbally, not SQL, assuming these were the only countries in the world, how would you calculate how many persons speak each language in the world?*/

/*Min umiddelbart tanke er at det kan jeg løse ved, at jeg fx skriver where language = Balochi, hvor jeg så kan se hvilke countrycode der hører til det language. Hvor mange mennesker der så snakker de forskellige languages, har jeg svært ved at finde ud af.*/

/*What actual information is missing? Where would you find it?*/

/*Jeg gætter at det vil være population fra tablen country.*/

/*
*Assignment Data.1
*/

/*Write an SQL statement that reads all countries that have 'Danish' as a language.*/

select countrycode
from countrylanguage
where language = 'Danish';

/*Write an SQL statement that shows what languages they speak in South Africa. Order them alphabetically.*/

select language
from countrylanguage
where countrycode = 'ZAF'
order by language;

/*Write an SQL statement that shows what languages they speak in 'UZB', 'TJK', and 'AFG'. Order them in descending order by percentage.*/

select language, percentage
from countrylanguage
where countrycode = 'UZB', 'TJK', 'AFG'
order by percentage;

/*
*Assignment Data.2
*/

/*Write an SQL statement that lists the head of state of all European countries.*/

select name, headofstate
from country;

/*Write an SQL statement that lists the code, name and population of countries in the region 'Southern and Central Asia'. Sort them by population.*/

select code, name, population
from country
where region = 'Southern and Central Asia'
order by population;

/*
*Assignment Data.3
*/

/*Please write a query with country name, country population, continent, name of capital, population of capital from all countries in Oceania, Antarctica, and South America. You must include countries that do not have a capital city.*/

select name, continent, count(name) capital, sum(population) 'Total population'
from country
group by continent;

/*
*Assignment Data.4
*/

/*Please write a query with country name, population, language, percentage, and percentage recalculated into number of people, for all languages in the countrylanguage table.*/

select name, population, language, percentage
from country, countrylanguage
/*Ved ikke hvordan den skal se ud*/

/*
*Assignment Data.5
*/

/*Using the SQL query from the previous assignment, augment it to group the languages.*/

/*Jeg forstår ikke hvad jeg skal udvide.*/

/*
*Assignment Data.6
*/

/*Please write an SQL query counting how many different languages are spoken in the world.*/

select continent, count(name) countries, sum(language) 'Total languages'
from country, countrylanguage
group by continent;

/*
*Assignment Data.7
*/

/*Please write an SQL query showing what cities have a namesake in another country.*/


/*Ved ikke hvordan den skal se ud*/
